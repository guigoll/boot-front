import React, { Component } from 'react';
import {Router, Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';

import { history} from './store/helpers';
import { alertActions } from './store/actions';
import { Login, Home , Register, Perfil, Layout} from './screens';

import { ProfileHeading } from './components/profile';
import { FeedList } from './components/feeds';
import { Media } from './components/media';
import { DashCamp } from './components/dash';
import {PrivateRoute } from './components/routes';

const AppRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
      <Perfil>
        <Component {...props} />
      </Perfil>
    )} />
  );

  class App extends Component {
    constructor(props) {
      super(props);
      const { dispatch } = this.props;
      history.listen((location, action) => {
        // clear alert on location change
        dispatch(alertActions.clear());
      });
    }
    render(){
      const { alert } = this.props;
      return(

        <Router history={history}>
          <Switch>
            <Route  path="/login"  component={Login} />
            <PrivateRoute  path="/home" component={Home} />
            <PrivateRoute path="/perfil" component={ProfileHeading}/>
            <AppRoute  path="/posts" component={FeedList}/>
            <AppRoute  path="/media" component={Media}/>
            <AppRoute   path="/dash" component={DashCamp}/>
          </Switch>
        </Router>
      );
    }
  }

  function mapStateToProps(state) {
    const { alert } = state;
    return {
      alert
    };
  }

  export  default connect(mapStateToProps)(App);
