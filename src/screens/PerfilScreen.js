import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Switch, Route } from 'react-router-dom';

import {FeedContent, FeedItem, FeedPost, FeedList } from '../components/feeds';
import { Navbar, NavbarBrand, NavbarNav, FormNavbar } from '../components/header';
import { MainContainer, Sidebar, Content} from '../components/main';
import {Button,   ButtonGroup} from '../components/button';
import {PerfilContent, AboutUser,  PhotoPerfil,  ProfileLayer, PerfilFollow} from '../components/perfil';
import {AboutUserContent, InfoUser} from '../components/aboutUser';
import {ProfileContent,ProfileHeading } from '../components/profile';
import {BannerLeft, BannerRight, BannerHeader, BannerImage} from '../components/marketing';
import {FollowTimeContent, FollowTimeItem} from '../components/followTimes'
import {Field, InputField, } from '../components/form';
import { Media } from '../components/media';
import { DashCamp } from '../components/dash';

const PerfilScreen = props =>({
  render(){
    const { user } = this.props;
    return(
      <div>
        <Navbar>
          <NavbarBrand />
          <NavbarNav>
            <FormNavbar>
              <Field
                component={InputField}
                type="search"
                className="form-control mr-sm-2"
                placeholder="Procurar jogador"
                />
            </FormNavbar>
          </NavbarNav>
        </Navbar>
        <MainContainer>
          <Sidebar>
            <PerfilContent>
              <ProfileLayer />
              <PhotoPerfil />
              <AboutUser
                UserName = {user.name}
                />
              <PerfilFollow />
            </PerfilContent>
            <AboutUserContent>
              <InfoUser />
            </AboutUserContent>
          </Sidebar>
          <Content>
             <ProfileHeading />
             <Switch>
               <Route  path="/posts" component={FeedList}/>
               <Route  path="/media" component={Media}/>
               <Route  path="/dash" component={DashCamp}/>
             </Switch>
          </Content>
          <Sidebar>
            <BannerLeft>
              <BannerHeader />
              <BannerImage />
            </BannerLeft>
            <FollowTimeContent>
              <FollowTimeItem />
            </FollowTimeContent>
          </Sidebar>
        </MainContainer>
      </div>
    );
  }
});

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user
  };
}

export default  connect(mapStateToProps)(PerfilScreen)
