import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import {Login, Avatar,  Form} from '../components/login';
import {Field, InputField} from '../components/form';
import {Button, ButtonIcon} from '../components/button';

import { Icon } from 'react-icons-kit'
import {facebook2, google2} from 'react-icons-kit/icomoon';

import {userActions} from '../store/actions';
import {validateName} from '../store/helpers';

class LoginScreen extends Component {

  constructor(props) {
    super(props);

    // reset login status
    this.props.dispatch(userActions.logout());

    this.state = {
      email: '',
      password: '',
      submitted: false
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    //console.log(e.target.value)
    this.setState({[e.target.name]: e.target.value})
  }

  onSubmit(e) {
    e.preventDefault();
    this.setState({ submitted: true });
    const { email, password } = this.state;
    const { dispatch } = this.props;
    if (email && password) {
      dispatch(userActions.login(email, password));
    }
  }
  render(){
    const { loggingIn } = this.props;
    const { email, password, submitted } = this.state;
    return(
      <Login>
        <Avatar/>
        <Form onSubmit={this.onSubmit}>
          <InputField
            type="text"
            name="email"
            validate={validateName}
            value={this.state.email}
            placeholder="Email"
            isValid={this.state.email.isValid}
            onChange={this.onChange}
            />
          <InputField
            type="password"
            name="password"
            validate={validateName}
            value={this.state.password}
            placeholder="Senha"
            onChange={this.onChange}
            />
          {submitted && !email &&
            <div className="help-block">Preencha o e-mail</div>
          }
          {submitted && !password &&
            <div className="help-block">Preencha a Senha</div>
          }
          <div className="form-group">
            <Button
              type="submit"
              className="btn btn-info btn-block login"
              Text="Login"
              />
            {loggingIn &&
              <img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
            }
          </div>
        </Form>
        <p className="text-center"><strong>Ou</strong></p>

        <ButtonIcon
          typre="submit"
          className="btn btn-face  btn-block login"
          Text="Continuar com o Facebook"
          Icon={<Icon icon={facebook2} size={20} />}
          />

        <ButtonIcon
          typre="submit"
          className="btn btn-google  btn-block login"
          Text="Continuar com o Google"
          Icon={<Icon icon={google2} size={20} />}
          />

        <div className="text-form-footer">
          <a href="#" className="text-center ">
            Esqueceu sua senha?
          </a>
          <hr />
          <Link to="/register" className="text-center criar">  Ainda não esta no Permata? Crie uma conta</Link>
        </div>
      </Login>
    );
  }
}

function mapStateToProps(state) {
  const { loggingIn } =state.authentication;
  return {
    loggingIn
  };
}

export  default connect(mapStateToProps)(LoginScreen);
