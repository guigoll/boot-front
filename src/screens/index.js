import  Login from './LoginScreen';
import Home from './HomeScreen';
import Perfil from './PerfilScreen';
import Register from './RegisterScreen';

export{
   Login,
   Home,
   Perfil,
   Register,
}
