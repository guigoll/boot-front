import React, {Component} from 'react';

const Button = ({
   className,
   type,
   ...props
}) => (
   <button
       className= { className}
       type={type}>
       { props.Text }</button>
);

export default Button;
