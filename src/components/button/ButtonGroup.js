import React, {Component} from 'react';

const ButtonGroup = ({
  className,
  type,
  ...props
}) => (
  <div className="input-group-append">
      <button
          className= { className}
          type="button">{ props.btnText }</button>  
  </div>
);

export default ButtonGroup;
