import React, {Component} from 'react';

const ButtonIcon = ({
  className,
  type,
  ...props
}) => (
  <button
    className= { className}
    type={type}>
    <div className="row">
      <div className="icon col-sm-2">{ props.Icon }</div>
      <div className="col-sm-5">
        { props.Text }
      </div>
    </div>
  </button>
);

export default ButtonIcon;
