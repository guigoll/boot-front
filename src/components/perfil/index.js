import PerfilContent from './PerfilContent';
import AboutUser from './AboutUser';
import PhotoPerfil from './PhotoPerfil';
import ProfileLayer from './ProfileLayer';
import PerfilFollow from './PerfilFollow';

export{
   PerfilContent,
   AboutUser,
   PhotoPerfil,
   ProfileLayer,
   PerfilFollow,
}
