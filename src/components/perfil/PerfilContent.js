import React, { Component} from 'react';

const PerfilContent = (props) =>(
  <div className="card">
    {props.children}
   </div>
);

export default PerfilContent;
