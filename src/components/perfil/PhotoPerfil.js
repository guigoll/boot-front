import React, {Component} from 'react';

const PhotoPerfil = () => (
  <div className="PhotoPerfil">
     <img src="../../images/perfil.png" className="rounded-circle mx-auto d-block" />
  </div>
);
export default PhotoPerfil;
