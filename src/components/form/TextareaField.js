import React, {Component} from 'react'
import { Icon } from 'react-icons-kit'
import {photo} from 'react-icons-kit/fa/photo'
import {statsBars2} from 'react-icons-kit/icomoon/statsBars2'
import {location} from 'react-icons-kit/icomoon/location'

const TextareaField = ({
  value,
  name,
  className,
  row,
  placeholder,
  id,
  onChange,
  onClick,
}) => (
  <div>
    <div className="painel">
      <div className="panel-body">
        <textarea
          id={id}
          className={className}
          value={value}
          name={name}
          row={row}
          placeholder = {placeholder}
          onChange={onChange}
          onClick={onClick}
          />
      </div>
      <div className="panel-footer">
        <ul className="list-inline list-left">
          <li className="list-inline-item">
            <a href="#" data-toggle="tooltip" data-placement="top" title="Adicionar foto">
              <Icon icon={photo} size={20} />
            </a>
          </li>
          <li className="list-inline-item">
            <a href="#" data-toggle="tooltip" data-placement="top" title="Adicionar enquete">
              <Icon icon={statsBars2} size={20} />
            </a>
          </li>
          <li className="list-inline-item">
            <a href="#" data-toggle="tooltip" data-placement="top" title="Adicionar localização">
              <Icon icon={location} size={20} />
            </a>
          </li>
        </ul>

        <ul className="list-inline list-right">
          <li className="list-inline-item">
            <button className="btn btn-outline-primary  btn-sm ">
               Publicar
            </button>
          </li>
        </ul>
      </div>
    </div>
  </div>
);

export default TextareaField;
