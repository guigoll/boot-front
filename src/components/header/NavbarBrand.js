import React, {Component } from 'react';

const NavbarBrand = () => (
  <div>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
         <span className="navbar-toggler-icon"></span>
      </button>
  </div>
);

export default NavbarBrand;
