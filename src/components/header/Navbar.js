import React, { Component}  from 'react';

const Navbar = (props) => (
    <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <div className="container">
         {props.children}
      </div>
    </nav>
);

export default Navbar;
