import React, {Component } from 'react';
import { Link } from 'react-router-dom';
import { Icon } from 'react-icons-kit'
import {envelopeO} from 'react-icons-kit/fa/envelopeO'
import {bellO} from 'react-icons-kit/fa/bellO'
import {home} from 'react-icons-kit/feather/home'

  const NavbarNav = (props) => (
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
     <ul className="navbar-nav mr-auto">
         <li className="nav-item">
            <Link to="/home" className="nav-link">
              <Icon icon={home} size={18} /><span className="nav-text">Pagina Inicial</span>
            </Link>
         </li>
         <li className="nav-item">
           <a href="#" className="nav-link ">
             <Icon icon={bellO} size={18} /><span className="nav-text">Notificações</span>
           </a>
         </li>
         <li className="nav-item">
           <a href="#" className="nav-link ">
             <Icon icon={envelopeO} size={18} /><span className="nav-text">Mensagens</span>
           </a>
         </li>
      </ul>
      {props.children}
      <ul className="navbar-nav">
         <li className="nav-item">
            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <img src= ".../../images/perfil.png"  className="img-perfil rounded-circle"/>
            </a>
            <div className="dropdown-menu  dropdown-menu-right">
                <a className="dropdown-item" href="#">Criar Grupo</a>
                <a className="dropdown-item" href="#">Novos Grupos</a>
              <div className="dropdown-divider"></div>
                <a className="dropdown-item" href="#">Registro de Atividades</a>
                <a className="dropdown-item" href="#">Configurações</a>
              <div className="dropdown-divider"></div>
                 <Link to="/login" className="dropdown-item">Sair</Link>
            </div>
         </li>
      </ul>
    </div>
  );

  export default NavbarNav;
