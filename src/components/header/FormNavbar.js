import React, {Component } from 'react';

const FormNavbar = (props) => (
   <div>
       <form className="form-inline my-2 my-lg-0">
           {props.children}      
        </form>
    </div>
);

export default FormNavbar;
