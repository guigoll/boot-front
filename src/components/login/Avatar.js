import React, { Component } from 'react';

const Avatar = () => (
      <div className="avatar">
         <img src=".../../images/logo.png" width="150" />
         <h2 className="logo-name">Permata</h2>
         <p className="text-center logo-subname">Encontre seu time e bora pro jogo.</p>
      </div>
);

export default Avatar ;
