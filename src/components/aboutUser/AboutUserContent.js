import React, {Component} from 'react';

const AboutContent = (props) => (
  <div className="card">
    <div className="card-body">
       {props.children }
    </div>
   </div>
);

export default AboutContent;
