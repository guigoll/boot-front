import InfoUser from './InfoUser';
import AboutUserContent from './AboutUserContent';

export {
  InfoUser,
  AboutUserContent,
}
