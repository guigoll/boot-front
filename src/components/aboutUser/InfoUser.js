import React, {Component} from 'react';
import { Modal } from '../modal';

import { Icon } from 'react-icons-kit'
import {ic_location_on} from 'react-icons-kit/md/ic_location_on'
import {ic_call_split} from 'react-icons-kit/md/ic_call_split'
import {ic_assignment} from 'react-icons-kit/md/ic_assignment'
import {ic_flag} from 'react-icons-kit/md/ic_flag'
import {arrowsV} from 'react-icons-kit/fa/arrowsV'
import {edit} from 'react-icons-kit/fa/edit'

class InfoUser extends Component{
  constructor(props){
    super(props)
    this.state = {show: false}

      this.showModal = this.showModal.bind(this);
      this.hideModal = this.hideModal.bind(this);
  }

  showModal(){
    this.setState({ show: true });
  }
  hideModal(){
    this.setState({ show: false });
  }

  render(){
  return (
      <div>
        <div className="desc">
          <div className="col-sm-6 text-left">
            <h6>Sobre</h6>
          </div>
          <div className="col-sm-6 text-right">
            <a href="#"  onClick={this.showModal} data-toggle="tooltip" data-placement="top" title="Editar">
              <Icon icon={edit} size={20} />
            </a>
          </div>
        </div>
        <ul className="list-about">
          <li>
            <span className="Icon">
              <Icon icon={ic_location_on} size={20} />
            </span>
            Mora em <a href="#">Americana, SP</a>
        </li>
        <li>
            <span className="Icon">
              <Icon icon={ic_call_split} size={20} />
            </span>
            Posição <a href="#">Centroavante</a>
        </li>
        <li>
          <span className="Icon">
            <Icon icon={arrowsV} size={20} />
          </span>
          Altura <a href="#"></a>
        </li>
        <li>
          <span className="Icon">
            <Icon icon={ic_assignment} size={20} />
          </span>
          Peso <a href="#">Kg</a>
        </li>
        <li>
          <span className="Icon">
            <Icon icon={ic_flag} size={20} />
          </span>
          Vinculo <a href="#">Seattle, WA</a>
        </li>
     </ul>

      <Modal show={this.state.show} handleClose={this.hideModal}>
        <p>Teste</p>
      </Modal>
   </div>
    );
  }
}

export default InfoUser;
