import React, {Component} from 'react';

const Sidebar = (props) => (
    <div className="col-sm-3">
        {props.children}       
    </div>
);

export default Sidebar;
