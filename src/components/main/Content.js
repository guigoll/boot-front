import React, { Component } from 'react';

const Content = (props) => (
    <div className="col-sm-6">
       {props.children}    
    </div>
);

export default Content;
