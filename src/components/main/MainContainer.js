import React, { Component } from 'react'

const MainContainer = (props) =>(
   <div className="container content">
     <div className="row">
       { props.children }
     </div>
   </div>
);

export default MainContainer;
