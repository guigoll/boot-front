import React, {Component} from 'react';

const FollowinTimeItem = () => (
    <div>
      <h6>Times <small>· <a href="#">View All</a></small></h6>
      <ul className="PhotoFollow list-group ">
        <li>
          <img src="../../images/perfil.png" className="rounded-circle mx-auto d-block" />
          <div className="PhotoFollowDesc">
            <strong>Jacob Thornton</strong> @fat
            <div>
              <button className="btn btn-outline-primary  btn-sm ">
                  Seguir
              </button>
            </div>
          </div>
        </li>
        <li>
          <img src="../../images/perfil.png" className="rounded-circle mx-auto d-block" />
          <div className="PhotoFollowDesc">
            <strong>Jacob Thornton</strong> @fat
            <div>
              <button className="btn btn-outline-primary  btn-sm ">
                  Seguir
              </button>
            </div>
          </div>
        </li>
      </ul>
    </div>
);

export default FollowinTimeItem;
