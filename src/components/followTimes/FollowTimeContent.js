import React, {Component} from 'react';

const FollowinTimeContent = (props) => (
  <div className="card">
    <div className="card-body">
       {props.children }
    </div>
   </div>
);

export default FollowinTimeContent;
