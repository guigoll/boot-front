import React, { Component } from 'react';

const FeedItem = (props) => (
    <div className="cx-post">
      {props.children}
    </div>
);

export default FeedItem;
