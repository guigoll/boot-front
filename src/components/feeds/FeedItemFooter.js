import React, {Component} from 'react';
import { Icon } from 'react-icons-kit'
import {commentO} from 'react-icons-kit/fa/commentO'
import {heartO} from 'react-icons-kit/fa/heartO'
import {envelopeO} from 'react-icons-kit/fa/envelopeO'
import {shareAlt} from 'react-icons-kit/fa/shareAlt'

const FeedItemFooter = () => (
  <div className="list-item-footer">
    <ul className="list-inline">
      <li className="list-inline-item" >
        <a href="#">
          <Icon icon={commentO}/>
        </a>
      </li>
      <li className="list-inline-item" >
        <a href="#">
          <Icon icon={shareAlt} />
        </a>
      </li>
      <li className="list-inline-item" >
        <a href="#">
          <Icon icon={heartO} />
        </a>
      </li>
      <li className="list-inline-item">
        <a href="#">
          <Icon icon={envelopeO} />
        </a>
      </li>
    </ul>
  </div>
);

export default FeedItemFooter;
