import React, { Component } from 'react';
import { connect } from 'react-redux';

import {Field,  InputPostField, TextareaField} from '../form';


class FeedPost extends Component{
  constructor(props){
    super(props)
    this.state = { post: false}

    this.changePost = this.changePost.bind(this);
  }
  changePost(){
    this.setState({ post: !this.state.post})
  }
  render(){
    return(
      <div>
        <Field
          component={this.state.post ? TextareaField: InputPostField}
          type="post"
          className="form-control border"
          placeholder="O que está acontecento?"
          onClick={this.changePost}
        />
    </div>
    );
  }
}

export default FeedPost;
