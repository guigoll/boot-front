import React, {Component} from 'react'

import FeedItemFooter from './FeedItemFooter';

const FeedList = ({
  ...props
}) => (
  <ul className="list-group">
    <div className="card-post-item">
      <li className="list-post">
        <div className="post-img">
          <img src="../../images/perfil.png" className="rounded-circle img-post" />
           <span className="post-name">Guilherme Henrique</span> <span className="post-nickname">@guigol .23 min</span>
        </div>
        <div className="post-content">
           <div className="text-justify">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla pretium, libero in tempus tincidunt, lectus urna laoreet leo, in fermentum ante leo id nisi. Suspendisse potenti. Nunc vel lorem scelerisque, mattis lectus quis, consectetur est. Maecenas vehicula dictum pellentesque. Vestibulum semper non magna vel hendrerit. Nulla aliquet lacus ligula, et ultrices velit facilisis et. Nam lobortis, sem vitae euismod fringilla, odio justo convallis augue, eget dignissim dui nunc vitae tellus. Aliquam sed laoreet risus.
              <img src="https://pbs.twimg.com/media/Dj8ibdEW0AAsyRS.jpg" className="img-fluid" />
           </div>
           <FeedItemFooter />
        </div>
      </li>
    </div>
  </ul>
);

export default FeedList;
