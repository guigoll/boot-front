import React, {Component} from 'react';

const FeedContent = (props) =>(
  <div className="card-post">
    <div className="cx-text">
      {props.children}
    </div>
  </div>
);

export default FeedContent;
