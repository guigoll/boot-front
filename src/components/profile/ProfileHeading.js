import React, { Component } from 'react';
import {NavLink} from "react-router-dom";

import { FeedList} from '../feeds';
import { Media } from '../media';
import { DashCamp } from '../dash';

class ProfileHeading extends Component{
  render(){
    return(
      <div className="cx-profile-heading">
         <ul className="list-inline">
           <li className="list-inline-item" >
             <NavLink to='/posts' activeClassName="selected" >Posts</NavLink>
           </li>
           <li className="list-inline-item" >
              <NavLink to='/media' activeClassName="is-active" >Media</NavLink>
           </li>
           <li className="list-inline-item" >
              <NavLink to='/dash' activeClassName="is-active" >Dash</NavLink>
           </li>
         </ul>
       </div>

    );
  }
}

export default ProfileHeading;
