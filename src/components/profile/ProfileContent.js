import React, {Component} from 'react';

const ProfileContent = (props) =>(
    <div className="profile-content">
      {props.children}
   </div>
);

export default ProfileContent;
