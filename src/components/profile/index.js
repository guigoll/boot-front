import ProfileHeading from './ProfileHeading';
import ProfileContent from './ProfileContent';

export {
  ProfileHeading,
  ProfileContent,
}
