import React, {Component} from 'react';
import { Route, Redirect } from 'react-router-dom';
import { Perfil } from '../../screens';
import { ProfileHeading } from '../profile';
import {FeedList } from '../feeds';
import { Media } from '../media';
import { DashCamp } from '../dash';

const PrivateSubRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
       <Perfil>
          <Component/>
      </Perfil>
   )} />
)

export default PrivateSubRoute;
