import React, {Component} from 'react';

const BannerImage = () => (
    <img className="card-img-top" src="https://bootstrap-themes.github.io/application/assets/img/instagram_2.jpg" alt="Card image cap" />
);

export default BannerImage;
