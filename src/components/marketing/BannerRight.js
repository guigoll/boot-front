import React, {Component} from 'react';

const BannerLeft = (props) =>(
  <div className="card">
    <div className="card-body">
     {props.children}
    </div>
  </div>
);

export default BannerLeft;
