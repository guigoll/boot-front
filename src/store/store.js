import {applyMiddleware, createStore} from 'redux';
import promise from 'redux-promise';
import multi from 'redux-multi';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { rootReducer } from './reducers';

const devTools =  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

export const store = applyMiddleware(logger, thunk, multi,promise)(createStore)(
  rootReducer,
  devTools
);
