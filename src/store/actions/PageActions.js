import { pageConstants } from '../constants';
import { alertActions } from './';
import { history } from '../helpers';

export const pageActions = {
  alterPage,
};

function alterPage(page){

  function request(user) { return { type: pageConstants.PAGE_REQUEST, page } }
  function success(user) { return { type: pageConstants.PAGE_SUCCESS, page} }
  function failure(error) { return { type: pageConstants.PAGE_FAILURE, error } }
}
