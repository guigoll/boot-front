import { AuthHeader } from '../helpers';


export const  teamService = {
     getAll,
     getById
}

function getAll(){
    const requestOptions = {
       method: 'GET',
       headers: {'Content-Type': 'application/json'}
    };
    return fetch('/teams', requestOptions).then(handleResponse);
}

function getById(id){
   const requestOptions = {
     method: 'GET',
     headers: {'Content-Type': 'application/json'}
   };

   fetch('/teams/${id}', requestOptions).then(handleResponse)
}
